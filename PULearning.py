class PULearner:
    def __init__(self, clf, repeat=10):
        self.clf = clf
        self.repeat = repeat
    
    def setup(self, df, feature_columns, target):
        self.df = df
        self.feature_columns = feature_columns
        self.target = target
    
    def run(self):
        print('Running with {} Repeat...'.format(str(self.repeat)))
        print('Initial Clasifier ...')

        self.unlabeled = self.df[self.df[self.target] == 0].copy()
        self.positive = self.df[self.df[self.target] == 1].copy()

        X_train = self.positive.append(self.unlabeled, ignore_index=True)[self.feature_columns]
        y_train = self.positive.append(self.unlabeled, ignore_index=True)[self.target]

        self.clf.fit(X_train, y_train)
        self.unlabeled['predict_proba'] = self.clf.predict_proba(self.unlabeled[self.feature_columns]).T[1]

        print('Getting Initial Reliable Negative ...')
        self.reliable_negative = self.unlabeled[self.unlabeled['predict_proba'] <= 0.3]
        self.unlabeled.drop(self.reliable_negative.index, inplace=True, axis=0)        

        print('Starting Repeat Improvement ...')
        print(self.reliable_negative)

        for i in range(self.repeat):
            n_unlabelled = self.unlabeled['Survived'].count()
            print('Number of Unlabelled: {}'.format(str(n_unlabelled)))

            if n_unlabelled == 0:
                print("All Unlabelled has been labelled, Stopping ...")
                break

            X_train = self.positive.append(self.reliable_negative, ignore_index=True)[self.feature_columns]
            y_train = self.positive.append(self.reliable_negative, ignore_index=True)[self.target]

            self.clf.fit(X_train, y_train)
            self.unlabeled['predict_proba'] = self.clf.predict_proba(self.unlabeled[self.feature_columns]).T[1]
            new_negative = self.unlabeled[self.unlabeled['predict_proba'] <= 0.3]
            
            print(new_negative)

            self.reliable_negative = self.reliable_negative.append(new_negative, ignore_index=True)
            self.unlabeled.drop(new_negative.index, inplace=True, axis=0)
        
        
